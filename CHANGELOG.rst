CHANGELOG
=========

This project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[0.10.0] - 2021-03-03
  - adding qspi controller to the SoC
  - adding qspi pins to the JA Pmod header of arty.
  - increased plic sources to 32 to accommodate interrupts from qspi
  - added bitfield based register diagrams
  - typo fixes in docs

[0.9.9] - 2021-02-08
  - updated block diagram of soc with spi cluster
  - fixed sphinx build issues with bibtex contrib library

[0.9.8] - 2021-02-06
  - lock bibtex contrib to 1.0.0 in docs/requirements.txt

[0.9.7] - 2020-09-25
  - Interface SPI to SoC

[0.9.6] - 2020-11-11
  - fixed stack space in crt to 12KB to fit within OCM of 16KiB
  
[0.9.5] - 2020-09-16
  - Interface PWM to SoC
  - Paramterized GPIO Interrupts
  - reduced gpio interrupts to 16
  - updated docs for pwms and gpios

[0.9.4] 2020-09-12
  - the xilinx tap module is not compatible with the latest riscv-copenocd

[0.9.3] - 2020-07-06
  - increased gpios to 22
  - remapped interrupt connections of gpios and uart.
  - updated docs to reflect more information about io signals and interrupt connections
  - fixed makefile targets for proper boot file generation.
  - fixed connect_gdb target and software/Makefile to point to the arty100t board alias

[0.9.2] - 2020-07-01
  - clarification of <path> and <jobs> in setup guide.
  - added resource summary for arty-100t board.
  - fixed typos in overview and memory map.
  - xilinx implementation is now perf-optimized.
  - generate_hexfile target to create bin folder if not already available.
  - version for each device updated in docs
  - added ci to check bsv build for merge requests

[0.9.1] - 2020-06-29
  - minor doc updates.


[0.9.0] - 2020-06-29
  - initial release of the Chromite M SoC with: clint, uart, gpio, ddr, plic, ocm and boot rom.

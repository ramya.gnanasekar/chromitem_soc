#####################
Licensing and Support
#####################

The IP modules and its components mentioned in this document
are provided at no cost and available under an Apache License, Version 2.0. 

For more information about support, customization and other IP developments
please write to info@incoresemi.com.

.. _pwm:

###################################
Pulse Width Modulation (PWM) Module
###################################

.. include:: <isonum.txt>

This chapter will discuss the operation of the Pulse width Modulation (PWM) module
instantiated in this design.

IP Details and Available Configuration
======================================


:numref:`PWM_ip_details` provides details of the source of the IP and and
details of the memory map.

.. tabularcolumns:: |l|C|

.. _PWM_ip_details:

.. table:: PWM IP details

  ========================================  ==============
  ..                                        **Value**
  ========================================  ==============
  Provider                                  gitlab
  Vendor                                    incoresemi
  Library                                   blocks/devices
  Version                                   1.1.2
  Ip Type                                   memory_mapped
  Numer of Config Registers                 5
  Direct Memory Region                      None
  Configuration Register Alignment (bytes)  4
  ========================================  ==============

:numref:`PWM_configuration_details` provides information of the various
parameters of the IP available at design time and their description

.. tabularcolumns:: |l|l|l|

.. _PWM_configuration_details:

.. table:: PWM IP Configuration Options

  ===============  ======================  ======================================================================================================================================================================================
  Configuration    Options                 Description
  ===============  ======================  ======================================================================================================================================================================================
  Bus interfaces   APB, AXI4L, AXI4        Choice of bus interface protocol supported on this IP
  Base address     Integer                 The base address where the memory map of the configuration register starts
  Bytes reserved   Integer >= 0X14         The number of bytes reserved for this instance. This can be much larger than the actual bytes required for the configuration registers but cannot be smaller than the value indicated.
  pwm_width        Integer > 16 and  < 64  An integer indicating the pwmwidth value at hardware configuration.
  channels         Integer > 1             An integer indicating the number of channels in a PWM Module
  outbar_en        Integer > 0 and  < 1    An integer indicating whether Complementary outputs are enabled (1) / disabled(0).
  ===============  ======================  ======================================================================================================================================================================================



PWM Instance Details
=====================



:numref:`PWM_instance_details` shows the values assigned to parameters of this
instance of the IP.

.. tabularcolumns:: |c|C|

.. _PWM_instance_details:

.. table:: PWM Instance Parameters and Assigned Values

  ====================  ====================
  **Parameter Name**    **Value Assigned**
  ====================  ====================
  Base Address          0X30000
  Bound Address         0X30100
  Bytes reserved        0X100
  Bus Interface         APB
  pwm_width             0X10
  channels              0X6
  outbar_en             0X0
  ====================  ====================


PWM Features
=============

The PWM module implements the standard PWM module with each channel having
interrupt line that gets connected to PLIC.

The following parameters can be configured in software:

* Start/Stop PWM: Using *PWM ST* in the ``Control Register``
* Prescalar: Using ``Clock Register``
* Clock Source: Using ``Clock Register``
* Duty Cycle: Using ``Duty Cycle Register``
* Period: Using: Using ``Period Register``
* Deadband delay: Using ``Deadband Delay Register``
* Interrupts: Using *Rise Interrupt*, *Fall Interrupt* & *Half period Interrupt* in the ``Control Register``
* Output Polarity: Using *OUT POL* in the ``Control Register``
* Output Inhibition: Using *OUT EN* or *OUTBAR EN* in the ``control Register``
* Channel Inhibition: Using *CH EN* in the ``control Register``
* Reset PWM: Using *PWM RST* in the ``control Register``

The following parameters can be configured in the hardware:

* The width of the Dutycycle and Period registers (``pwmwidth``)
* Number of channels in each PWM Module (``channels``)
* Enable or disable Complementary Output for all channel of each PWM Module (``outbar_en``)

This PWM supports *OUTBAR_EN* only when :

* Hardware supports the ``outbar_en`` configured in hardware.


Register Map
============



The register map for the PWM control registers is shown in
:numref:`PWM_register_map`. 

.. tabularcolumns:: |l|c|c|c|l|

.. _PWM_register_map:

.. table:: PWM Register Mapping for all configuration registes

  +-----------------+---------------+--------------+--------------+-----------------------------------------------------------+
  | Register-Name   | Offset(hex)   |   Size(Bits) | Reset(hex)   | Description                                               |
  +=================+===============+==============+==============+===========================================================+
  | Clock           | 0X0           |           16 | 0X0          | Various control fields to manipulate pwm clock parameters |
  +-----------------+---------------+--------------+--------------+-----------------------------------------------------------+
  | Control         | 0X4           |           16 | 0X0          | Holds various control & interrupt status fields           |
  +-----------------+---------------+--------------+--------------+-----------------------------------------------------------+
  | period          | 0X8           |           16 | 0X0          | Holds Period value of the channel                         |
  +-----------------+---------------+--------------+--------------+-----------------------------------------------------------+
  | duty cycle      | 0XC           |           16 | 0X0          | Holds duty cycle of the channel                           |
  +-----------------+---------------+--------------+--------------+-----------------------------------------------------------+
  | deadband delay  | 0X10          |           16 | 0X0          | Holds Deadband value of the channel                       |
  +-----------------+---------------+--------------+--------------+-----------------------------------------------------------+

All addresses not mentioned in the above table within ``Base Address`` and
``Bound Address`` are reserved and accesses to those regions will generate a
slave error on the bus interface





The register access attributes for the PWM control registers are shown in 
:numref:`PWM_register_access_attr`.

.. tabularcolumns:: |l|c|c|c|C|

.. _PWM_register_access_attr:

.. table:: PWM Register Access Attributes for all configuration Registers

  ===============  =============  ============  ============  ============
  Register-Name    Access Type    Reset Type    Min Access    Max Access
  ===============  =============  ============  ============  ============
  Clock            read-write     synchronous   2B            4B
  Control          read-write     synchronous   2B            4B
  period           read-write     synchronous   2B            4B
  duty cycle       read-write     synchronous   2B            4B
  deadband delay   read-write     synchronous   2B            4B
  ===============  =============  ============  ============  ============




CLOCK Register
==============

This is a 16-bit register which allows manipulation of the clock parameters. By choosing the clock source and prescalar values. It's unique to all channels of this module.

.. bitfield::
    :bits: 16
    :lanes: 2
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CLK_SOURCE", "attr":"read-write" },
    {"bits": 15, "name": "PRESCALAR", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _Clock_subfields:

.. table:: Clock subfeild description

  +--------+--------------+-------------+------------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                              |
  +========+==============+=============+==========================================+
  | [0:0]  | clk_source   | read-write  | Selects clock source external / internal |
  +--------+--------------+-------------+------------------------------------------+
  | [15:1] | prescalar    | read-write  | Defines the clock divisor value.         |
  +--------+--------------+-------------+------------------------------------------+





CONTROL Register
================

This is a read-write register holding various control bit. For each channel,
the control should be written to

.. math:: base_address + ((channel << 4) + 0x04)

The following table provides information of the various bits and their description"

.. bitfield::
    :bits: 16
    :lanes: 2
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CH_EN", "attr":"read-write" },
    {"bits": 1, "name": "PWM_ST", "attr":"read-write" },
    {"bits": 1, "name": "OUT_EN", "attr":"read-write" },
    {"bits": 1, "name": "OUT_POL", "attr":"read-write" },
    {"bits": 1, "name": "PWM_RST", "attr":"read-write" },
    {"bits": 1, "name": "OUTBAR_EN", "attr":"read-write" },
    {"bits": 1, "name": "HF_PRD_INT_EN", "attr":"read-write" },
    {"bits": 1, "name": "FALL_INT_EN", "attr":"read-write" },
    {"bits": 1, "name": "RISE_INT_EN", "attr":"read-write" },
    {"bits": 1, "name": "HF_PRD_INT", "attr":"read-only" },
    {"bits": 1, "name": "FALL_INT", "attr":"read-only" },
    {"bits": 1, "name": "RISE_INT", "attr":"read-only" },
    {"bits": 1, "name": "LOAD_PWM", "attr":"read-write" },
    {"bits": 3, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _Control_subfields:

.. table:: Control subfeild description

  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name    | Attribute   | Description                                                                                                                                                                               |
  +=========+===============+=============+===========================================================================================================================================================================================+
  | [0:0]   | CH_EN         | read-write  | Controls if the channel is enabled(1) / disabled(0).                                                                                                                                      |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [1:1]   | PWM_ST        | read-write  | Controls the PWM Modules running status started(1) / stopped(0).                                                                                                                          |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [2:2]   | OUT_EN        | read-write  | Controls if the output of pwm channel is enabled(1) / disabled(0).                                                                                                                        |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [3:3]   | OUT_POL       | read-write  | Controls the polarity Normal(0) / Inverted(1).                                                                                                                                            |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [4:4]   | PWM_RST       | read-write  | Resets the PWM Counter                                                                                                                                                                    |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [5:5]   | OUTBAR_EN     | read-write  | Controls if the complementaty output of pwm channel is enabled(1) / disabled(0). It should be noted that, this bit has significance only if the hardware module supports `outbar_en` = 1. |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [6:6]   | HF_PRD_INT_EN | read-write  | Controls if the Half Period Interrupt of channel is enabled(1) / disabled(0).                                                                                                             |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [7:7]   | FALL_INT_EN   | read-write  | Controls if the Fall Interrupt of channel is enabled(1) / disabled(0).                                                                                                                    |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [8:8]   | RISE_INT_EN   | read-write  | Controls if the Rise Interrupt of channel is enabled(1) / disabled(0).                                                                                                                    |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [9:9]   | HF_PRD_INT    | read-only   | Indicates if Half Period interrupt is triggered.                                                                                                                                          |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [10:10] | FALL_INT      | read-only   | Indicates if Fall interrupt is triggered.                                                                                                                                                 |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [11:11] | RISE_INT      | read-only   | Indicates if RISE interrupt is triggered.                                                                                                                                                 |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [12:12] | LOAD_PWM      | read-write  | Loads the duty cycle and Period register values to the PWM Module.                                                                                                                        |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [15:13] | Reserved      | read-write  | Reads will return zeros and writes will have no effect                                                                                                                                    |
  +---------+---------------+-------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+





PERIOD Register
===============

This is a read-write register of ``pwmwidth`` defined in hardware configuration.
Writing to this register enqueues the period of the particular channel. For each channel,
the period should be written to

.. math:: base_address + ((channel << 4) + 0x08)

.. bitfield::
    :bits: 16
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "PERIOD", "attr":"read-write" }]


DUTY CYCLE Register
===================

This is a read-write register of ``pwmwidth`` defined in hardware configuration.
Writing to this register enqueues the duty cycle of the particular channel. For each channel,
the period should be written to

.. math:: base_address + ((channel << 4) + 0x0C)

.. bitfield::
    :bits: 16
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "DUTY CYCLE", "attr":"read-write" }]


DEADBAND DELAY Register
=======================

This is a  16-bit read-write register that enqueues the deadband delay of a
particular channel. For each channel, the period should be written to

.. math:: base_address + ((channel << 4) + 0x10)

.. bitfield::
    :bits: 16
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "DEADBAND DELAY", "attr":"read-write" }]


IO and Sideband Signals
=======================



The following table describes the io-signals generated from this IP that may
directly or indirectly drive certain IO pads.

.. tabularcolumns:: |l|l|l|l|

.. _PWM_io_signals:

.. table:: PWM generated IO signals

  ===================  ======  ===========  ===============================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ===============================================================
  pwm_o                     1  output       channel Output
  pwm_comp                  1  output       Complementary  channel Output (Exists only if `outbar_en` = 1 )
  ===================  ======  ===========  ===============================================================

.. note:: some of these signals may be muxed with other functional IO from different
  ips and users should refer to any pinmux module available on chip





.. tabularcolumns:: |l|l|l|l|

.. _PWM_sb_signals:

.. table:: PWM generated side-band signals generated

  ===================  ======  ===========  ======================================================================================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ======================================================================================================================
  interrupt                 6  output       Signal indicating an interrupt has been raised by the PWM if enabled in control register. Signal is connected to PLIC.
  ===================  ======  ===========  ======================================================================================================================


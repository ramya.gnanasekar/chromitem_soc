.. _spi:

#########################################
Serial Peripheral Interface (SPI) Module
#########################################

.. include:: <isonum.txt>

This chapter will discuss the operation of the Serial Peripheral Interface (SPI) module instantiated in this design.

IP Details and Available Configuration
======================================


:numref:`SPI_ip_details` provides details of the source of the IP and and
details of the memory map.

.. tabularcolumns:: |l|C|

.. _SPI_ip_details:

.. table:: SPI IP details

  ========================================  ==============
  ..                                        **Value**
  ========================================  ==============
  Provider                                  gitlab
  Vendor                                    incoresemi
  Library                                   blocks/devices
  Version                                   1.2.0
  Ip Type                                   memory_mapped
  Numer of Config Registers                 10
  Direct Memory Region                      None
  Configuration Register Alignment (bytes)  4
  ========================================  ==============

:numref:`SPI_configuration_details` provides information of the various
parameters of the IP available at design time and their description

.. tabularcolumns:: |l|l|l|

.. _SPI_configuration_details:

.. table:: SPI IP Configuration Options

  ===============  ====================  ======================================================================================================================================================================================
  Configuration    Options               Description
  ===============  ====================  ======================================================================================================================================================================================
  Bus interfaces   APB, AXI4L, AXI4      Choice of bus interface protocol supported on this IP
  Base address     Integer               The base address where the memory map of the configuration register starts
  Bytes reserved   Integer >= 0X18       The number of bytes reserved for this instance. This can be much larger than the actual bytes required for the configuration registers but cannot be smaller than the value indicated.
  slave_count      Integer > 0 and  < 8  An integer indicating number of slaves to be controlled by SPI module.
  ===============  ====================  ======================================================================================================================================================================================



SPI Instance Details
=====================



:numref:`SPI_instance_details` shows the values assigned to parameters of this
instance of the IP.

.. tabularcolumns:: |c|C|

.. _SPI_instance_details:

.. table:: SPI Instance Parameters and Assigned Values

  ====================  ====================
  **Parameter Name**    **Value Assigned**
  ====================  ====================
  Base Address          0X20100
  Bound Address         0X201FF
  Bytes reserved        0XFF
  Bus Interface         APB
  slave_count           0X1
  ====================  ====================


SPI Features
=============

The SPI module implements the standard SPI communication in master only configuration supporting:

* Custom Setup / Hold / Inter-Transfer delays
* 16-bit Prescalar for selecting Baudrate
* CRC based Hashing for Rx/Tx Data
* Bit-controlled data transfer
* Interrupt line that gets connected to PLIC.

The following parameters can be configured in software:

* Clock Phase and Polarity using ``CPHA`` && ``CPOL``
* Communication mode using ``DPLX``
* Setup, Hold & Inter Transfer delays using ``SPI_DR`` Registers
* CRC of Transmitted and Received Data
* Interrupt to write AND/OR Read from TxR and RxR respectively.
* Bit wise control of data Tx/Rx.

The following parameters can be configured in the hardware:

* Number of slaves that can be controlled using the SPI module (``slave_count``)

Register Map
============



The register map for the SPI control registers is shown in
:numref:`SPI_register_map`. 

.. tabularcolumns:: |l|c|c|c|l|

.. _SPI_register_map:

.. table:: SPI Register Mapping for all configuration registes

  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | Register-Name   | Offset(hex)   |   Size(Bits) | Reset(hex)   | Description                                                       |
  +=================+===============+==============+==============+===================================================================+
  | SPI_CR1         | 0X0           |           32 | 0X0          | Various fields to control SPI parameters                          |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | SPI_CR2         | 0X4           |           32 | 0X0          | Various fields to control secondary SPI parameters                |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | SPI_SR          | 0X8           |           32 | 0X0          | Various fields that indicate status of SPI module.                |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | TXR             | 0XC           |           32 | 0X0          | Register to Holds data that needs to be sent over SPI             |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | RXR             | 0X10          |           32 | 0X0          | Register to Hold data that is received over the SPI               |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | RX_CRC          | 0X14          |           32 | 0X0          | Register to store so far received data's CRC (only on CRC_EN)     |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | TX_CRC          | 0X18          |           32 | 0X0          | Register to store so far transmitted data's CRC (only on CRC_EN). |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | SPI_DR          | 0X1C          |           32 | 0X0          | Register to control delay parameters to support slow slaves.      |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | SPI_PSCR        | 0X20          |           16 | 0X0          | Prescalar value that divides bus clock and provides `sclk`        |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+
  | SPI_CRCPR       | 0X24          |           32 | 0X0          | CRC Initial Value & Polynomial to use with CRC module             |
  +-----------------+---------------+--------------+--------------+-------------------------------------------------------------------+

All addresses not mentioned in the above table within ``Base Address`` and
``Bound Address`` are reserved and accesses to those regions will generate a
slave error on the bus interface





The register access attributes for the SPI control registers are shown in 
:numref:`SPI_register_access_attr`.

.. tabularcolumns:: |l|c|c|c|C|

.. _SPI_register_access_attr:

.. table:: SPI Register Access Attributes for all configuration Registers

  ===============  =============  ============  ============  ============
  Register-Name    Access Type    Reset Type    Min Access    Max Access
  ===============  =============  ============  ============  ============
  SPI_CR1          read-write     synchronous   4B            4B
  SPI_CR2          read-write     synchronous   4B            4B
  SPI_SR           read-only      synchronous   4B            4B
  TXR              read-write     synchronous   4B            4B
  RXR              read-only      synchronous   4B            4B
  RX_CRC           read-only      synchronous   2B            4B
  TX_CRC           read-only      synchronous   2B            4B
  SPI_DR           read-write     synchronous   4B            4B
  SPI_PSCR         read-write     synchronous   2B            4B
  SPI_CRCPR        read-write     synchronous   4B            4B
  ===============  =============  ============  ============  ============




SPI_CR1 Register
================

This is a 32-bit register which allows primary controls of SPI module including Phase, Polarity, communication mode, Bits to transmit/receive, peripheral enable & Rx/Tx controls.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CPHA", "attr":"read-write" },
    {"bits": 1, "name": "CPOL", "attr":"read-write" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "RX_STRT", "attr":"read-write" },
    {"bits": 1, "name": "HYB_STRT", "attr":"read-write" },
    {"bits": 1, "name": "SPE", "attr":"read-write" },
    {"bits": 1, "name": "LSB_1ST", "attr":"read-write" },
    {"bits": 1, "name": "SSI", "attr":"read-write" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "CLR_STATE", "attr":"read-write" },
    {"bits": 1, "name": "CRC_LEN", "attr":"read-write" },
    {"bits": 1, "name": "CRC_NEXT", "attr":"read-write" },
    {"bits": 1, "name": "CRC_EN", "attr":"read-write" },
    {"bits": 2, "name": "DPLX", "attr":"read-write" },
    {"bits": 8, "name": "TX_BITS", "attr":"read-write" },
    {"bits": 8, "name": "RX_BITS", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_CR1_subfields:

.. table:: SPI_CR1 subfeild description

  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                                                |
  +=========+==============+=============+============================================================================================================+
  | [0:0]   | cpha         | read-write  | Selects clock phase for data capture                                                                       |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [1:1]   | cpol         | read-write  | Selects clock polarity for data capture                                                                    |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [3:2]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                     |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [4:4]   | rx_strt      | read-write  | Enable (1) / Disable (0) Reception in Full Duplex, Transmit (0) / Receive (1) in Half Duplex & Simplex,    |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [5:5]   | hyb_strt     | read-write  | Enable (1) / Disable (0) Transmission in Full Duplex Receive immediately after Transmit (1) in Half Duplex |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [6:6]   | spe          | read-write  | Enable SPI-peripheral for transaction                                                                      |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [7:7]   | lsb_1st      | read-write  | Bit transfer in LSB(1) / MSB(0) First                                                                      |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [8:8]   | ssi          | read-write  | Slave select input (0)/(1) - Relevant on Rx only scenario                                                  |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [9:9]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                     |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [10:10] | clr_state    | read-write  | Clears the internal FIFO and resets Tx/Rx States                                                           |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [11:11] | crc_len      | read-write  | CRC Length 8-bit (0) / 16-bit (1)                                                                          |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [12:12] | crc_next     | read-write  | Next Transmit value from Tx (0) / Rx(1) Buffer                                                             |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [13:13] | crc_en       | read-write  | CRC Check Enable                                                                                           |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [15:14] | dplx         | read-write  | Selects mode of Communication Simplex (01), Half Duplex(10), Full Duplex (11)                              |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [23:16] | tx_bits      | read-write  | to transmit (1-254) for bit transmission, 255 for unlimited data transfer                                  |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+
  | [31:24] | rx_bits      | read-write  | to receive (1-254) for bit reception, 255 for unlimited data receive.                                      |
  +---------+--------------+-------------+------------------------------------------------------------------------------------------------------------+





SPI_CR2 Register
================

This is a 32-bit register which allows secondary controls of SPI module including DMA controls, Interrupt Controls, Idle pulse state, CRC controls, Slave management, Thresholds.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 3, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "SS_PULS", "attr":"read-write" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "ERR_IE", "attr":"read-write" },
    {"bits": 1, "name": "RXNE_IE", "attr":"read-write" },
    {"bits": 1, "name": "TXE_IE", "attr":"read-write" },
    {"bits": 4, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "FIFO_TH", "attr":"read-write" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "CRC_RIN", "attr":"read-write" },
    {"bits": 1, "name": "CRC_ROUT", "attr":"read-write" },
    {"bits": 3, "name": "SLV_ID", "attr":"read-write" },
    {"bits": 12, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_CR2_subfields:

.. table:: SPI_CR2 subfeild description

  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                   |
  +=========+==============+=============+===============================================================================+
  | [2:0]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                        |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [3:3]   | ss_puls      | read-write  | SS Goes High [1] or Keeps Low [0] during inter-transfer delay (See SPI_DR[3]) |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [4:4]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                        |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [5:5]   | err_ie       | read-write  | Enable Interrupt for Errors                                                   |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [6:6]   | rxne_ie      | read-write  | Enable Interrupt when RxR is NOT empty                                        |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [7:7]   | txe_ie       | read-write  | Enable Interrupt when TxR is empty                                            |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [11:8]  | Reserved     | read-write  | Reads will return zeros and writes will have no effect                        |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [12:12] | fifo_th      | read-write  | Sets Threshold for FIFO (0) Half (1) 3/4th's                                  |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [14:13] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                        |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [15:15] | crc_rin      | read-write  | Reflect the data fed for CRC - www.crccalc.com                                |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [16:16] | crc_rout     | read-write  | Reflect the CRC output - www.crccalc.com                                      |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [19:17] | slv_id       | read-write  | Slave Selector (SSN) where N=0 to 7 (depends on the 'slave_count' in IP)      |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [31:20] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                        |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+





SPI_SR Register
===============

32-bit register that indicates status of SPI module, including the status to write/read TxR/RxR respectively along with internal FIFO status.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "RXNE", "attr":"read-only" },
    {"bits": 1, "name": "TXE", "attr":"read-only" },
    {"bits": 1, "name": "RXF", "attr":"read-only" },
    {"bits": 1, "name": "TXNF", "attr":"read-only" },
    {"bits": 1, "name": "CRCERR", "attr":"read-only" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "OVRF", "attr":"read-only" },
    {"bits": 1, "name": "BSY", "attr":"read-only" },
    {"bits": 5, "name": "FRCNT", "attr":"read-only" },
    {"bits": 5, "name": "FTCNT", "attr":"read-only" },
    {"bits": 14, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_SR_subfields:

.. table:: SPI_SR subfeild description

  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                       |
  +=========+==============+=============+===================================================================================+
  | [0:0]   | rxne         | read-only   | RxR is not empty (when set, Ready to read RxR)                                    |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [1:1]   | txe          | read-only   | TxR is empty (when set, Ready in writing to TxR)                                  |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [2:2]   | rxf          | read-only   | RxFIFO is full                                                                    |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [3:3]   | txnf         | read-only   | TxFIFO is not full (Data from TxR will be enqueued as long as this field is set ) |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [4:4]   | crcerr       | read-only   | CRC Error                                                                         |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [5:5]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                            |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [6:6]   | ovrf         | read-only   | Overflow Flag (Tx FIFO overflow)                                                  |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [7:7]   | bsy          | read-only   | SPI is performing Transactions (1) / IDLE (0)                                     |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [12:8]  | frcnt        | read-only   | Unread bytes in Rx FIFO ( Indicates number of bytes filled in FIFO)               |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [17:13] | ftcnt        | read-only   | Yet- to -transmit bytes in Tx FIFO ( Indicates number of bytes filled in FIFO)    |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [31:18] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                            |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+





TXR Register
============

32-bit register that should be written for data transmission. Data should be written to this register when `txe` is set and each write is aligned according to the `lsb_1st`. As long as `txnf` is set, data can be written into this register that will be queued into TxFIFO whose filled status can be found from `ftcnt`.

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "TXR", "attr":"read-write" }]


RXR Register
============

32-bit Read-Only register that should be read during data reception. Data should be read from this register when `rxne` is set and each read is aligned according to the `lsb_1st`. It is best practice to keep track of `frcnt` to know how many bytes are yet to be read from internal RxFIFO. If the FIFO fills up `rxf` will be set (ending the receive transaction)

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "RXR", "attr":"read-only" }]


RX_CRC Register
===============

32-bit register that'll be updated with CRC hash of data received. The Hash will depend on `crc_poly`, `crc_init`, `crc_en`, `crc_len`, `crc_rin` & `crc_rout` parameters. Intermediate hashes are stored into this register periodically. Final hash can be read from this register when transaction winds up.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "RXCRC", "attr":"read-only" },
    {"bits": 16, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _RX_CRC_subfields:

.. table:: RX_CRC subfeild description

  +---------+--------------+-------------+--------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                            |
  +=========+==============+=============+========================================================+
  | [15:0]  | rxcrc        | read-only   | CRC Hash for data received during Rxn                  |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [31:16] | Reserved     | read-write  | Reads will return zeros and writes will have no effect |
  +---------+--------------+-------------+--------------------------------------------------------+





TX_CRC Register
===============

32-bit register that'll be updated with CRC hash of data transmitted. The Hash will depend on `crc_poly`, `crc_init`, `crc_en`, `crc_len`, `crc_rin` & `crc_rout` parameters. Intermediate hashes are stored into this register periodically. Final hash can be read from this register when transaction winds up.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "TXCRC", "attr":"read-only" },
    {"bits": 16, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _TX_CRC_subfields:

.. table:: TX_CRC subfeild description

  +---------+--------------+-------------+--------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                            |
  +=========+==============+=============+========================================================+
  | [15:0]  | txcrc        | read-only   | CRC Hash for data received during Txn                  |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [31:16] | Reserved     | read-write  | Reads will return zeros and writes will have no effect |
  +---------+--------------+-------------+--------------------------------------------------------+





SPI_DR Register
===============

This 32-bit register holds 'additional' delay that should be provided to support slow-responding slaves, whose 'setup', 'hold' & 'xfer' delay should be assisted by master. This register provides option of choosing clock source and the delay enabling bits for quick enable and disable of particular delay. It should be noted that, during the xfer delay, nss follows the `ss_puls`.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CLK_SRC", "attr":"read-write" },
    {"bits": 1, "name": "SD_EN", "attr":"read-write" },
    {"bits": 1, "name": "HD_EN", "attr":"read-write" },
    {"bits": 1, "name": "XD_EN", "attr":"read-write" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 9, "name": "SETUP_DELAY", "attr":"read-write" },
    {"bits": 9, "name": "HOLD_DELAY", "attr":"read-write" },
    {"bits": 1, "name": "XFER_DELAY", "attr":"read-write" },
    {"bits": 8, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_DR_subfields:

.. table:: SPI_DR subfeild description

  +---------+--------------+-------------+--------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                            |
  +=========+==============+=============+========================================================+
  | [0:0]   | clk_src      | read-write  | Selects bus clock(0) / SPI clock(1) for delays         |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [1:1]   | sd_en        | read-write  | Enable Setup delay                                     |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [2:2]   | hd_en        | read-write  | Enable Hold Delay                                      |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [3:3]   | xd_en        | read-write  | Enable Transfer Delay (Delay after transaction)        |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [4:4]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [13:5]  | setup_delay  | read-write  | 9 bit delay for setup time                             |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [22:14] | hold_delay   | read-write  | 9-bit delay for Hold time                              |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [23:23] | xfer_delay   | read-write  | 9-bit delay between transactions (idles to SS_PULS)    |
  +---------+--------------+-------------+--------------------------------------------------------+
  | [31:24] | Reserved     | read-write  | Reads will return zeros and writes will have no effect |
  +---------+--------------+-------------+--------------------------------------------------------+





SPI_PSCR Register
=================

The prescalar value is the divisor value for clock. Prescalar minimum value is 2 and maximum value is 65534.
..note:: Prescalar should be an even value. if odd value is sent, it will be added '+1' to make it even.

.. bitfield::
    :bits: 16
    :lanes: 2
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "PRESCALAR", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_PSCR_subfields:

.. table:: SPI_PSCR subfeild description

  +--------+--------------+-------------+-----------------------------------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                                                     |
  +========+==============+=============+=================================================================+
  | [15:0] | prescalar    | read-write  | value > 1 and  < 65535 are valid values for dividing bus clock. |
  +--------+--------------+-------------+-----------------------------------------------------------------+





SPI_CRCPR Register
==================

This 32-bit register holds the value of polynomial and initial values according to the `crc_len` that dictates between CRC8 / CRC16

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "CRC_POLY", "attr":"read-write" },
    {"bits": 16, "name": "CRC_INIT", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_CRCPR_subfields:

.. table:: SPI_CRCPR subfeild description

  +---------+--------------+-------------+---------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                             |
  +=========+==============+=============+=========================================================+
  | [15:0]  | crc_poly     | read-write  | Polynomial to be used for generating CRC Hash           |
  +---------+--------------+-------------+---------------------------------------------------------+
  | [31:16] | crc_init     | read-write  | Initial value of CRC Algo. to be used to generate Hash. |
  +---------+--------------+-------------+---------------------------------------------------------+





IO and Sideband Signals
=======================



The following table describes the io-signals generated from this IP that may
directly or indirectly drive certain IO pads.

.. tabularcolumns:: |l|l|l|l|

.. _SPI_io_signals:

.. table:: SPI generated IO signals

  ===================  ======  ===========  =============
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  =============
  mosi                      1  output       SPI Output
  miso                      1  input        SPI Input
  sclk                      1  output       Serial Clock
  nss                       1  output       Slave select
  ===================  ======  ===========  =============

.. note:: some of these signals may be muxed with other functional IO from different
  ips and users should refer to any pinmux module available on chip





.. tabularcolumns:: |l|l|l|l|

.. _SPI_sb_signals:

.. table:: SPI generated side-band signals generated

  ===================  ======  ===========  ================================================================================================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ================================================================================================================================
  interrupt                 1  output       Signal indicating an interrupt has been raised by the SPI if enabled in control register (SPI_CR2). Signal is connected to PLIC.
  ===================  ======  ===========  ================================================================================================================================



PLIC Interrupt Mapping
======================

.. tabularcolumns:: |l|l|l|

.. _PLIC_interrupt_mapping:

.. table:: PLIC interrupt mapping from devices

   ============ =========== =========================================
   Interrupt ID Peripheral  Description
   ============ =========== =========================================
   0            GPIO-0      GPIO peripheral Interrupt from pin0
   1            GPIO-1      GPIO peripheral Interrupt from pin1
   2            GPIO-2      GPIO peripheral Interrupt from pin2
   3            GPIO-3      GPIO peripheral Interrupt from pin3
   4            GPIO-4      GPIO peripheral Interrupt from pin4
   5            GPIO-5      GPIO peripheral Interrupt from pin5
   6            GPIO-6      GPIO peripheral Interrupt from pin6
   7            GPIO-7      GPIO peripheral Interrupt from pin7
   8            GPIO-8      GPIO peripheral Interrupt from pin8
   9            GPIO-9      GPIO peripheral Interrupt from pin9
   10           GPIO-10     GPIO peripheral Interrupt from pin10
   11           GPIO-11     GPIO peripheral Interrupt from pin11
   12           GPIO-12     GPIO peripheral Interrupt from pin12
   13           GPIO-13     GPIO peripheral Interrupt from pin13
   14           GPIO-14     GPIO peripheral Interrupt from pin14
   15           GPIO-15     GPIO peripheral Interrupt from pin15
   16           UART-0      UART peripheral Interrupt.
   17           PWM0-0      PWM0 peripheral Interrupt from channel 0
   18           PWM0-1      PWM0 peripheral Interrupt from channel 1
   19           PWM0-2      PWM0 peripheral Interrupt from channel 2
   20           PWM0-3      PWM0 peripheral Interrupt from channel 3
   21           PWM0-4      PWM0 peripheral Interrupt from channel 4
   22           PWM0-5      PWM0 peripheral Interrupt from channel 5
   23           SPI -0      SPI0 peripheral Interrupt
   24           SPI -1      SPI1 peripheral Interrupt
   25           SPI -2      SPI2 peripheral Interrupt
   26           QSPI0       Timeout flag interrupt
   27           QSPI0       Status Match Flag
   28           QSPI0       FIFO Threshold Flag
   29           QSPI0       Transfer Complete Flag
   30           QSPI0       Transfer Error Flag
   31           QSPI0       Request Ready Flag
   ============ =========== =========================================


